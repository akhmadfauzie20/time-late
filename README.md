## HOW TO SETUP
1. Installing PHP dependencies by running `composer install`
2. Copy `.env.example` to `.env` and fills required info
3. Data setup by running `php artisan migrate:fresh --seed`

## USER GUIDE
1.First, as usual application , we must login first

<p align="center"><img src="assets/img/login-page.jpg" alt="Login Page">

2.In the login page itself has an attendance function that allows users absent, without going through the login first,
in this view we need the uid or user id of each registered user or employee 

<p align="center"><img src="assets/img/absent-page.jpg" alt="Absent Page">

3.After logging in, we will immediately be directed to the dashboard, which displays lateness data from each user or employee every month,
in addition, in this view we can filter the delays every year

<p align="center"><img src="assets/img/dashboard-page.jpg" alt="Dashboard Page">

4.After that, we can press the employee sidebar, which is on the left of the display to go to the employee display

<p align="center"><img src="assets/img/pegawai-page.jpg" alt="Pegawai Page">

5.Then, if you log in as admin, more or less the display will be like the one above, and we can click the add data button to add or create employee data

<p align="center"><img src="assets/img/adding-pegawai.jpg" alt="Pegawai Page">

6.After the data is made, we can directly log in using the account, maybe beforehand I will tell you the view to edit the data, and print the employee data display

<p align="center"><img src="assets/img/pegawai-add.response.jpg" alt="Pegawai Page Response">

7.After the data is created, we can click the edit button on the table and edit data as we need
and then just click submit or save button , and walla , the data is success being edit.

<p align="center"><img src="assets/img/pegawai-edit.jpg" alt="Pegawai Edit">

<p align="center"><img src="assets/img/data-after-edit-pegawai.jpg" alt="Edit Response">

8.And then, after we add data, we can print employee data that was registered earlier

<p align="center"><img src="assets/img/print-pegawai.jpg" alt="Print Pegawai">

9.After all of that, we can return to the login page to simulate the page attendance that we have filled with the user id of the user that we created earlier

<p align="center"><img src="assets/img/absent-rules.jpg" alt="Absent Pegawai">

<p align="center"><img src="assets/img/absent-response.jpg" alt="Absent Response">

10.Finally, after we are absent, the data will be stored in the delay table, and will appear on the dashboard in the form of adding an existing chart, or will create a new chart.

<p align="center"><img src="assets/img/keterlambatan-page.jpg" alt="Keterlambatan Page">

11.The Keterlambatan table also has a print function like the one in the employee table.

<p align="center"><img src="assets/img/keterlambatan-print.jpg" alt="Keterlambatan Print Page">

12.Bonus, this application also has an activity function, or notification function to users who are late, or to HRD

<p align="center"><img src="assets/img/activity-page.jpg" alt="Activity Page">

13.For see full picture , you can visit my gitlab

<a href="https://gitlab.com/akhmadfauzie20/time-late">Visit</a>






