<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware'=> ['guest']], function () {
    /* Login */
Route:: get('/', 'Auth\LoginController@index')->name('login');
Route:: get('/absent', 'Auth\LoginController@absent')->name('login.absent');
Route:: post('/login', 'Auth\LoginController@showLoginForm');
Route:: post('/login/auth', 'Auth\LoginController@authenticate');

Route::post('keterlambatan/absent', 'KeterlambatanController@absent')->name('keterlambatan.absent');

    /* Forget Password */
Route:: get('/forget', 'Auth\ForgotPasswordController@index')->name('forget');
Route:: post('/forgot-password/auth', 'Auth\ForgotPasswordController@create');

});

Route::group(['middleware'=> ['auth', 'revalidate']], function() {
    Route::get('/logout/auth', 'Auth\LoginController@logout')->name('logout');

/* Dashboard */
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/activity', 'DashboardController@activity')->name('activity');

/* Home */
    Route::get('/home', 'HomeController@index')->name('home');

/* Pegawai */
Route::group(['prefix'=> 'pegawai'], function() {
    Route::get('/', 'PegawaiController@index')->name('pegawai');
    Route::get('/search', 'PegawaiController@indexOf')->name('pegawai.search');
    Route::get('/create', 'PegawaiController@create');
    Route::post('/store', 'PegawaiController@store');
    Route::get('/edit/{id}', 'PegawaiController@edit');
    Route::get('/pegawai-bulk', 'PegawaiController@bulk')->name('pegawai-bulk');
    Route::put('/update/{id}', 'PegawaiController@update');
    Route::delete('/delete/{id}', 'PegawaiController@destroy');
});

/*Keterlambatan */
Route::group(['prefix'=> 'keterlambatan'], function() {
    Route::get('/', 'KeterlambatanController@index')->name('keterlambatan');
    Route::get('/search', 'KeterlambatanController@indexOf')->name('keterlambatan.search');
    Route::get('/create', 'KeterlambatanController@create');
    Route::post('/store', 'KeterlambatanController@store');
    Route::get('/edit/{id}', 'KeterlambatanController@edit');
    Route::get('/keterlambatan-bulk', 'KeterlambatanController@bulk')->name('keterlambatan.bulk');
    Route::put('/update/{id}', 'KeterlambatanController@update');
    Route::delete('/delete/{id}', 'KeterlambatanController@destroy');
});

});
