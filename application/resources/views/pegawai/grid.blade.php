@extends('layouts.app')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        #data_length {
        'margin-top': -4px;
        'margin-bottom': 10px;
        }

        ul.pagination {
            float: right;
            margin-top: -22px;
            margin-right: -17px;
        }
    </style>
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">Pegawai</h3>
            <div class="row">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                    <div class="panel">
                        <br>
                        @php
                            $no = 1;
                            $sesi = Auth::user();
                        @endphp

                        @if($sesi['role'] == 1)
                            <div class="panel-heading" style="padding-bottom: 0px;">
                                <a href="{{ route('pegawai-bulk', array_merge(app('request')->all())) }}" class="btn btn-danger btn-md" style="margin-top: -24px; margin-right: 10px;float: left;" target="_blank"><i
                                        class="fa fa-print"></i> Cetak Data
                                </a>
                                <hr>
                            </div>
                            <div class="panel-body">
                                <a href="{{ url('pegawai/create') }}" class="btn btn-primary pull-right" style="margin-top: -77px;">
                                <span>
                                    Tambah Data
                                </span>
                                </a>
                                @else
                                    <div class="panel-heading" style="padding-top: 0px; padding-bottom: 0px;">
                                        <h3 class="panel-title">Table Data Pegawai</h3>
                                        <a href="{{ route('pegawai-bulk', array_merge(app('request')->all())) }}" class="btn btn-danger btn-md" style="margin-top: -24px;
                                      margin-right: 10px;
                                      float: right;" target="_blank"><i
                                                class="fa fa-print"></i>
                                            Cetak Data
                                        </a>
                                        <hr>
                                    </div>
                                    <div class="panel-body">
                                    @endif
                                        @if(count($data) < 10)
                                            <form action="{{ route('pegawai.search', array_merge(app('request')->all()) )}}" method="GET" class="form-inline" style="margin-left: auto">
                                                <div class="input-group"
                                                     style="float: right;
                                                            box-shadow: none !important;
                                                            margin-top:-38px;
                                                            margin-bottom: 44px;">
                                                    <div class="input-group" style="box-shadow: none !important; margin-bottom:-60px;">
                                        @else
                                            <form action="{{ route('pegawai.search', array_merge(app('request')->all()) )}}" method="GET" class="form-inline" style="margin-left: auto">
                                                <div class="input-group"
                                                     style="float: right;
                                                            box-shadow: none !important;
                                                            margin-top:-38px;
                                                            margin-bottom: 15px;">
                                                    <div class="input-group" style="box-shadow: none !important; margin-bottom:-60px;">
                                                    @endif
                                                        <input autocomplete="off" type="text" class="form-control" name="q" value="{{ app('request')->input('q', '') }}" placeholder="Search Name , jabatan"
                                                               style="max-width: 128px;
                                                                      margin-top:-4px;">
                                                        <div class="input-group-btn">
                                                            <button class="btn btn-search btn-primary"
                                                                    style="border-radius: 30px
                                                                           30px 30px 30px !important;
                                                                           margin-left: 15px;
                                                                           margin-right: 5px;
                                                                           margin-top: -4px;">
                                                                    Search
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            @if($data->count())
                                                <table id="data" class="table table-striped table-bordered" width="100%" cellspacing="0">
                                                    <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>UID (User ID)</th>
                                                        <th>Nama Pegawai</th>
                                                        <th>Jabatan</th>
                                                        <th>Foto</th>
                                                        <th>Opsi</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                    @foreach($data as $datas)
                                                        <td>{{ $no++ }}</td>
                                                        <td>{{ $datas->UID }}</td>
                                                        <td>{{ $datas->name }}</td>
                                                        <td>{{ $datas->jabatan }}</td>
                                                        <td>{{ $datas->file }}</td>
                                                        @if($sesi['role'] == 1)
                                                            <td>
                                                                <form action="{{ url('pegawai/delete', $datas->id) }}" method="post">
                                                                    {{ csrf_field() }} {{ method_field('DELETE') }}
                                                                    <a href="{{ url('pegawai/edit',$datas->id) }}" class=" btn btn-sm btn-primary">Edit</a>
                                                                    <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
                                                                </form>
                                                            </td>

                                                        @elseif($sesi['role'] == 0)
                                                                <td>
                                                                    @if($datas->id == Auth::user()->id_user)
                                                                        <a href="{{ url('pegawai/edit',$datas->id) }}" class=" btn btn-sm btn-primary">Edit</a>
                                                                    @endif
                                                                </td>
                                                                @endif
                                                    </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            @else
                                                <i class="fa fa-exclamation-triangle"></i> Data Tidak Tersedia
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
    <script>
        $(document).ready(function() {
            $('#data').DataTable({
                "searching": false,
                @if(count($data) < 10)
                "paginate": false
                @endif
            });
        });
    </script>

                    <!-- END MAIN CONTENT -->
@endsection
