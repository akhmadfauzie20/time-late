@extends('layouts.app')
@section('content')

    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">Pegawai</h3>
            <div class="row">
                <div class="col-md-12">
                    <!-- INPUTS -->
                    @foreach($data as $datas)
                    @php
                        $sesi = Auth::user();
                    @endphp
                    <form action="{{ url('pegawai/update', $datas->id) }}" method="post" enctype="multipart/form-data">
                        <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Edit Data Pegawai</h3>
                            <hr>
                        </div>
                            <div class="panel-body row" style="margin-top: -45px;">
                                <div class="col-md-6 col-xs-12" style="margin-left: 15px;">
                                    @csrf {{ csrf_field() }} {{method_field('PUT')}}
                                        <div class="form-group">
                                            <h4><label for="name">User ID</label></h4>
                                            <input type="number" class="form-control" id="uid" name="uid" placeholder="Masukkan Nomer UID" style="height: 45px" value="{{ $datas->UID }}" required>
                                        </div>
                                        <div class="form-group">
                                            <h4><label for="name">Nama Pegawai</label></h4>
                                            <input type="text" class="form-control" id="name" name="name" placeholder="Masukkan Nama Pegawai" style="height: 45px" value="{{ $datas->name }}" onkeypress="return hanyaHuruf(event)" required>
                                        </div>
                                        <div class="form-group">
                                            <h4><label for="jabatan">Jabatan</label></h4>
                                            @if($sesi['role'] == 1)
                                                <input type="text" class="form-control" name="jabatan" placeholder="Masukkan Jabatan" style="height: 45px" onkeypress="return hanyaHuruf2(event)" value="{{ $datas->jabatan }}" required>
                                            @else
                                                <input type="text" class="form-control" name="jabatan" placeholder="Masukkan Jabatan" style="height: 45px" onkeypress="return hanyaHuruf2(event)" value="{{ $datas->jabatan }}" required readonly>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <h4><label for="name">Email</label></h4>
                                            <input type="text" class="form-control" id="email" name="email" placeholder="Masukkan Email" style="height: 45px" value="{{ $datas->email }}" required>
                                        </div>
                                        <div class="form-group">
                                        <h4><label for="password">Password</label></h4>
                                        <div class="input-group" id="show_hide_password">
                                            <input class="form-control" type="password" id="password" name="password" value="{{ $datas->password }}">
                                            <div class="input-group-addon">
                                                <a href="#" onclick="showPass()"><i class="fa fa-eye-slash" aria-hidden="true" id="icon"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12" style="width: 48%; margin-right: -1px;">
                                    <div class="form-group">
                                        <h4><label for="exampleFormControlFile1" style="margin-top: 2px; margin-left: 78px;">Foto Lama</label></h4>
                                        <img src="{{ url('uploads/file/'.$datas->file) }}" style="width: 64%; margin-top: -3px; height: 325px; margin-left: 72px;">
                                        <input type="file" accept="image/*" class="form-control-file" id="file" name="file" style="margin-top: 7px; margin-left: 73px;" value="{{ $datas->file }}">
                                    </div>
                                </div>
                                <div class="btn-group" style="float: right; margin-top: 10px;margin-right: 17px;">
                                    <a href="{{url('pegawai')}}" class="btn btn-danger">Cancel</a>
                                    <button class="btn btn-primary" type="submit" style="margin-left: 10px;">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    @endforeach
                <!-- END INPUTS -->
                </div>
            </div>
        </div>
    </div>

@endsection
<script type="text/javascript">
    function hanyaHuruf(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode != 46 && charCode != 32 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122))
            return false;
        return true;
    }

    function hanyaHuruf2(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode != 32 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122))
            return false;
        return true;
    }

    function showPass() {
        var pass = document.getElementById("password");
        var btn = document.getElementById("icon");
        if(pass.type == "password") {
            pass.type = "text";
            btn.classList.remove('fa-eye-slash')
            btn.classList.add('fa-eye');

        } else {
            pass.type = "password";
            btn.classList.remove('fa-eye')
            btn.classList.add('fa-eye-slash');

        }

    }
</script>

