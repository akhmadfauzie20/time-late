@extends('layouts.app')
@section('content')

    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">Pegawai</h3>
            <div class="row">
                <div class="col-md-12">
                    <!-- INPUTS -->
                    <div class="panel">
                        <div class="panel-heading" style="margin-bottom: -38px;">
                            <h3 class="panel-title">Tambah Data Pegawai</h3><hr>
                        </div>
                        <div class="panel-body row">
                            <form action="{{ url('pegawai/store') }}" method="post" enctype="multipart/form-data">
                                <div class="col-md-6 col-xs-12">
                                    @csrf
                                        <div class="form-group">
                                            <h4><label for="uid">User ID</label></h4>
                                            <input type="number" class="form-control" name="uid" placeholder="Masukkan No UID" style="height: 45px" value="<?php echo '3578' . rand(1000000, 9999999)?>" readonly>
                                        </div>
                                        <div class="form-group">
                                            <h4><label for="name">Nama Pegawai</label></h4>
                                            <input type="text" class="form-control" name="name" placeholder="Masukkan Nama Pegawai" style="height: 45px" onkeypress="return hanyaHuruf(event)" required>
                                        </div>
                                        <div class="form-group">
                                            <h4><label for="jabatan">Jabatan</label></h4>
                                            <input type="text" class="form-control" name="jabatan" placeholder="Masukkan Jabatan" style="height: 45px" onkeypress="return hanyaHuruf2(event)" required>
                                        </div>
                                    <div class="form-group">
                                        <h4><label for="exampleFormControlFile1">Upload Foto</label></h4>
                                        <input type="file" accept="image/*" class="form-control-file" id="file" name="file" style="margin-top:25px;" required>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <h4><label for="email">Email</label></h4>
                                        <input type="email" class="form-control" name="email" placeholder="Masukkan Email" style="height: 45px" required>
                                    </div>
                                    <div class="form-group">
                                        <h4><label for="jabatan">Password</label></h4>
                                        <div class="input-group" id="show_hide_password">
                                            <input type="password" class="form-control" name="password" id="pass" placeholder="Masukkan Password" style="height: 45px" required>
                                            <div class="input-group-addon">
                                                <a href="#" onclick="showPass()"><i class="fa fa-eye-slash" aria-hidden="true" id="icon"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h4><label for="jabatan">Role</label></h4>
                                        <select class="form-control" name="role" style="height: 45px;" required>
                                            <option value="1" hidden=""></option>
                                            <option value="1" selected>Admin</option>
                                            <option value="0">Pegawai</option>
                                        </select>
                                    </div>
                                    <div class="btn-group" style="float: right; margin-top: 43px; margin-right: 15px;">
                                        <a href="{{url('pegawai')}}" class="btn btn-danger">Cancel</a>
                                        <button type="submit" class="btn btn-primary" style="margin-left: 10px;">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END INPUTS -->
                </div>
            </div>
        </div>
    </div>
@endsection

<script type="text/javascript">
    function hanyaHuruf(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode != 46 && charCode != 32 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122))
            return false;
        return true;
    }

    function hanyaHuruf2(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode != 32 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122))
            return false;
        return true;
    }

    function showPass() {
        var pass = document.getElementById("pass");
        var btn = document.getElementById("icon");
        if(pass.type == "password") {
            pass.type = "text";
            btn.classList.remove('fa-eye-slash')
            btn.classList.add('fa-eye');

        } else {
            pass.type = "password";
            btn.classList.remove('fa-eye')
            btn.classList.add('fa-eye-slash');

        }

    }

</script>
