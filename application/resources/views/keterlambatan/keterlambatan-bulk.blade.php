<!DOCTYPE html>
<html>

<head>
    <title>Data Keterlambatan</title>
    <style>
        body,
        * {
            font-family: sans-serif;
            font-size: 12px;
        }

        h1 {
            font-size: 24px;
        }

        .bold {
            font-weight: bold;
        }

        .center {
            text-align: center;
        }

        .spacer {
            padding: 12px;
        }

        .table {
            width: 100%;
            border-collapse: collapse;
        }

        .table.table-bordered tr td {
            border: 1px solid #333;
            padding: 8px 8px;
        }
    </style>
</head>
<body>
    @foreach($datas as $data)
    <div style="width: 640px; margin: 0 auto;">
        <u><h1 class="center">Data Keterlambatan</h1></u>
        <table class="table table-bordered">
            <tr>
                <td class="bold">Nama Pegawai :</td>
                <td>{{ ($data->name) }}</td>
            </tr>
            <tr>
                <td class="bold">Jabatan :</td>
                <td>{{ ($data->jabatan) }}</td>
            </tr>
            <tr>
                <td class="bold">Bulan :</td>
                <td>{{ ($data->bulan) }}</td>
            </tr>
            <tr>
                <td class="bold">Tahun :</td>
                <td>{{ ($data->tahun) }}</td>
            </tr>
            <tr>
                <td class="bold">Terlambat :</td>
                <td>{{ ($data->telat) }} Menit</td>
            </tr>
        </table>
    </div>
    <br>
    <hr>
    @endforeach

<script>
    window.print();
    setTimeout(function() {
        window.close();
    }, 500);
</script>
</body>

</html>
