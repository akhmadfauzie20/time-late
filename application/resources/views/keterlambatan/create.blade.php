@extends('layouts.app')

@section('content')
<div class="main-content">
    <div class="container-fluid">
        <h3 class="page-title">Keterlambatan</h3>
        <div class="row">
            <div class="col-md-12">
                <!-- INPUTS -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Tambah Data Keterlambatan</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-6 col-xs-12" style="margin-right: 20px;">

                        </div>
                        <form action="{{ url('keterlambatan/store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="receipient-name" class="form-control-label">Nama Pegawai :</label>
                                <select class="form-control" id="nama" name="name" style="width: 100%; height: 45px;" required="">
                                    <option value="" hidden="">Pilih Nama Pegawai</option>
                                    @foreach($data as $datas)
                                    <option value="{{$datas->id}}">{{ $datas->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="recipient-name" class="form-control-label">Tanggal :</label>
                                <select class="form-control" id="tanggal" name="tanggal" style="width: 100%;height: 45px;" required="">
                                    <option value="" hidden>Pilih Tanggal</option>
                                    <?php
                                    $tgl_skr = date('d');
                                    for ($d = 1; $d < 32; $d++) {
                                    ?>
                                    <option value="<?php echo $d ?>">
                                        <?php echo $d ?>
                                    </option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="recipient-name" class="form-control-label">Bulan :</label>
                                <select class="form-control" id="bulan" name="bulan" style="width: 100%;height: 45px;" required="">
                                    <option value="" hidden>Pilih Bulan</option>
                                    <option value="1">Januari</option>
                                    <option value="2">Februari</option>
                                    <option value="3">Maret</option>
                                    <option value="4">April</option>
                                    <option value="5">Mei</option>
                                    <option value="6">Juni</option>
                                    <option value="7">Juli</option>
                                    <option value="8">Agustus</option>
                                    <option value="9">September</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">November</option>
                                    <option value="12">Desember</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="form-control-label">Tahun :</label>
                                <select class="form-control" name="tahun" id="tahun" style="width: 100%;height: 45px;">
                                    <option value="" hidden>Pilih Tahun</option>
                                    <?php
                                    $thn_skr = date('Y');
                                    for ($x = $thn_skr; $x >= 2012; $x--) {
                                    ?>
                                    <option value="<?php echo $x ?>">
                                        <?php echo $x ?>
                                    </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="form-control-label">Telat (Menit) :</label>
                                <input type="text" class="form-control" id="telat" name="telat" autocomplete="off" placeholder="Masukkan Waktu Terlambat" style="height: 45px" maxlength="3" onkeypress="return hanyaAngka(event)" required="">
                            </div>
                            <br>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="{{url('keterlambatan')}}" class="btn btn-danger">Cancel</a>
                        </form>
                    </div>
                </div>
                <!-- END INPUTS -->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#nama').select2({
            placeholder: "Pilih Nama Pegawai",
              theme: "bootstrap"
        });
    });
    function hanyaAngka(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))

    return false;
    return true;
}
</script>
@endsection
