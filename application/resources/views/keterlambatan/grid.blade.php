@extends('layouts.app') @section('content')

<meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        ul.pagination {
            float: right;
            margin-top: -41px;
        }
    </style>
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">Keterlambatan</h3>
            <div class="row">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                    <div class="panel">
                        <br>
                        <div class="panel-heading" style="padding-top: 0px; padding-bottom: 0px;">
                            <h3 class="panel-title">Table Data Keterlambatan</h3>
{{--                            <a href="{{ url('keterlambatan/create') }}" class=" btn btn-sm btn-primary">Create</a>--}}
                            <a href="{{ route('keterlambatan.bulk', array_merge(app('request')->all())) }}" class="btn btn-danger btn-md" style="margin-top: -25px; margin-right: 10px;float: right;" target="_blank"><i
                                    class="fa fa-print"></i> Cetak Keterlambatan
                            </a>
                            <hr>
                        </div>
                        @php
                            $no = 1;
                            $sesi = Auth::user();
                        @endphp
                        @if(count($data) < 10)
                            <div class="panel-body" style="margin-top: 15px;">
                                <form action="{{ route('keterlambatan.search', array_merge(app('request')->all()) )}}" method="GET" class="form-inline search" style="margin-left: auto;margin-bottom: 10px;">
                                    <div class="input-group" style="float: right; box-shadow: none !important; margin-top:-70px;">
                        @else
                        <div class="panel-body" style="margin-top: -15px;">
                            <form action="{{ route('keterlambatan.search', array_merge(app('request')->all()) )}}" method="GET" class="form-inline search" style="margin-left: auto;margin-bottom: 10px;">
                                <div class="input-group" style="float: right; box-shadow: none !important; margin-top:-36px;">
                                    @endif
                                    <div class="input-group" style="box-shadow: none !important;  margin-bottom:-92px;">
                                        <input autocomplete="off" type="text" class="form-control" name="q" value="{{ app('request')->input('q', '') }}" placeholder="Search Name , jabatan" style="max-width: 128px;
                                              border-radius: 30px 0 0 30px
                                              !important;">
                                        <select class="form-control" placeholder="Filter Tahun" name="tahun" id="tahun" style="display: inline-block;
                                                   width: 122px;
                                                   border-radius: 0 0 0 0 !important;
                                                   padding:1px 15px;
                                                   height: 34px;
                                                   margin-bottom: 10px" id="statusFilter">
                                            <option value="" hidden="">Pilih Tahun</option>
                                            <option value="2016">2016</option>
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                        </select>
                                        <div class="input-group-btn">
                                            <button class="btn btn-search btn-primary" style="border-radius: 30px
                                              30px 30px 30px !important; margin-left: 15px; margin-right: 11px; margin-top: -9px;">Search
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <table id="data" class="table table-striped table-bordered" cellspacing="0">
                                @if($data->count())
                                    <thead>
                                    <tr>
                                        <th>NO</th>
                                        <th>UID</th>
                                        <th>Nama Pegawai</th>
                                        <th>Jabatan</th>
                                        <th>Tanggal</th>
                                        <th>Bulan</th>
                                        <th>Tahun</th>
                                        <th>Telat (Menit)</th>
                                        @if($sesi['role'] == 1)
                                            <th>Opsi</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        @foreach($data as $datas)
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $datas->UID }}</td>
                                            <td>{{ $datas->name }}</td>
                                            <td>{{ $datas->jabatan }}</td>
                                            <td>{{ $datas->tanggal }}</td>
                                            <td>{{ $datas->bulan }}</td>
                                            <td>{{ $datas->tahun}}</td>
                                            <td>{{ $datas->telat}}</td>
                                            @if($sesi['role'] == 1)
                                            <td>
                                                <form action="{{ url('keterlambatan/delete', $datas->id_keterlambatan) }}" method="post">
                                                    {{ csrf_field() }} {{ method_field('DELETE') }}
{{--                                                    <a href="{{ url('keterlambatan/edit',$datas->id_keterlambatan) }}" class=" btn btn-sm btn-primary">Edit</a>--}}
                                                    <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data?')">Delete</button>
                                                </form>
                                            </td>
                                            @endif
                                    </tr>
                                    @endforeach
                                    </tbody>
                            </table>
                            @else
                                <i class="fa fa-exclamation-triangle"></i> Data Tidak Tersedia
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
            <div class="row">
                <div class="col-md-6">
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>

</div>
</div>
</div>
</div>
</div>
</div>
    <script>
        $(document).ready(function() {
            $('#data').DataTable({
                "searching": false,
                @if(count($data) < 10)
                    "paginate":false
                @endif
            });
            $('#search').select(function () {
                this.data.write()
            });
        });
    </script>
    <!-- END MAIN CONTENT -->
@endsection
