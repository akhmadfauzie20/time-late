@extends('layouts.app') @section('content')
<div class="main-content">
    <div class="container-fluid">
        <h3 class="page-title">Keterlambatan</h3>
        <div class="row">
            <div class="col-md-12">
                <!-- INPUTS -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Edit Data Keterlambatan</h3>
                    </div>
                    @foreach($data as $datas)
                    <div class="panel-body">
                        <form action="{{ url('keterlambatan/update', $datas->id_keterlambatan) }}" method="post" enctype="multipart/form-data">
                            @csrf {{ csrf_field() }} {{method_field('PUT')}}
                            <div class="form-group">
                                <label for="recipient-name" class="form-control-label">Nama Pegawai :</label>
                                <select class=" select form-control" id="nama2" name="name" readonly>
                                    <option value="{{$datas->id}}" readonly>{{ $datas->name}}</option>
                                    @foreach($pegawai as $data)
                                    <option value="{{$data->id}}" readonly>{{ $data->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="recipient-name" class="form-control-label">Bulan :</label>
                                <select class="form-control" id="bulan" name="bulan" style="width: 100%;height: 45px;" required="">
                                    <option value="{{$datas->bulan}}" hidden=""></option>
                                    <option selected="{{$datas->bulan}}" hidden>{{$datas->bulan}}</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    <option>6</option>
                                    <option>7</option>
                                    <option>8</option>
                                    <option>9</option>
                                    <option>10</option>
                                    <option>11</option>
                                    <option>12</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="form-control-label">Tahun :</label>
                                <select class="form-control" name="tahun" id="tahun" style="width: 100%;height: 45px;">
                                    <option selected="{{$datas->tahun}}" hidden>{{$datas->tahun}} </option>
                                    <?php
                                        $thn_skr = date('Y');
                                            for ($x = $thn_skr; $x >= 2012; $x--) {
                                    ?>
                                        <option value="<?php echo $x ?>">
                                            <?php echo $x ?>
                                        </option>
                                        <?php
                                        }?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="form-control-label">Telat :</label>
                                <input type="text" class="form-control" id="telat" name="telat" autocomplete="off" style="width: 100%;height: 45px;" value="{{$datas->telat}}" onkeypress="return hanyaAngka(event)" maxlength="3" readonly>
                            </div>
                            <br>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="{{url('keterlambatan')}}" class="btn btn-danger">Cancel</a>
                        </form>
                    </div>
                </div>
                @endforeach
                <!-- END INPUTS -->
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#nama2').select2({
            theme: "bootstrap"
        });
    });
    $(".select option").val(function(idx, val) {
        $(this).siblings('[value="'+ val +'"]').remove();
    return val;
    });
     function hanyaAngka(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))

    return false;
    return true;
}
</script>
@endsection
