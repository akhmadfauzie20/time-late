<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Login | TimeLate</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link href="{{asset('assets/metronic/assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/metronic/assets/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/extends/main.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/mdb.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/mdb.lite.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/mdb.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome -->
    <link href="{{asset('assets/css/fontawesome/all.css')}}" rel="stylesheet" type="text/css" />
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="assets/css/main.css">
    <!-- ICONS -->
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favico.png">
</head>

<body>
<!-- WRAPPER -->
<div id="wrapper">
    <div class="vertical-align-wrap">
        <div class="vertical-align-middle">
            <div class="auth-box ">
                <div class="left">
                    <div class="content">
                        <div class="header">
                            <div class="logo text-center"><img src="assets/img/logo-time-late.png" alt="TimeLate Logo"></div>
                            <p class="lead" style="font-size: medium;">Login to your account
                        </div>
                        <form class="form-auth-small" action="" id="form-login">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Password">
                                <input type="hidden" name="current_date" value="<?php date_default_timezone_set('Asia/Jakarta'); echo 'Indonesian Timezone:' . date('h:i:s M d, Y'); ?>" readonly="readonly">
                            </div>
                            <div class="form-group clearfix">
                                <label class="fancy-checkbox element-left">
                                    <input type="checkbox">
                                    <span {{ old( 'remember') ? 'checked' : '' }}>Remember me</span>
                                </label>
                            </div>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">LOGIN</button>
                            <div class="bottom">
                                <span class="helper-text"><i class="fa fa-user"></i> <a href="#" data-toggle="modal" data-target="#modalLoginForm">Absent</a></span>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="right">
                    <div class="content text">
                        <h1 class="heading" style="color: black;">"Time is the most valuable thing a man can spend."</h1>
                        <p>–Theophrastus.</p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Absent Page</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="POST" id="absent">
                {{ csrf_field() }}
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        <i class="fas fa-user prefix black-text"></i>
                        <input type="text" id="uid" name="uid" class="form-control validate" placeholder="Your User ID">
                        <span class="text-danger">{{ $errors->first('uid') }}</span>
                    </div>
                    <div class="md-form mb-4">
                        <i class="fa fa-lock prefix black-text"></i>
                        <input type="password" id="password" name="password" class="form-control validate" placeholder="Your password">
                        <span class="text-danger">{{ $errors->first('password') }}</span>
                    </div>

                    <div class="alert alert-success d-none" id="msg_div">
                        <span id="res_message"></span>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button type="submit" id="sent_form" class="btn btn-md btn-primary">Absent</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- END WRAPPER -->
<script src="{{asset('assets/metronic/assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
{{--	<script src="{{asset('assets/metronic/assets/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>--}}
<script src="{{asset('assets/metronic/assets/snippets/pages/user/login.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/fontawesome/all.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/mdb.js')}}" type="text/javascript"></script>
<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>

<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        var email, password, btn_loader;
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        if ($("#absent").length > 0) {
            $("#absent").validate({
                rules: {
                    uid: {
                        required: true,
                        digits:true,
                        maxlength:11
                    },
                    password: {
                        required:true,
                    },
                },
                messages: {
                    uid: {
                        required: "Please input your User ID",
                        digits:"Please input a number only!",
                        maxlength:"Max length is 11 digits"
                    },
                    password: {
                        required: "Please input your valid password",
                    },
                },
                submitHandler: function (form) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $('#sent_form').html('Loading...');
                    $.ajax({
                        url: 'keterlambatan/absent',
                        type: "POST",
                        data: $('#absent').serializeArray(),
                        success: function (response) {
                            // console.log('loh kok jalan anjing ilang goblok');
                            // console.log(response.msg);
                            $('#sent_form').html('Success');
                            $('#res_message').show();
                            $('#res_message').html(response.msg);
                            $('#msg_div').removeClass('d-none');

                            document.getElementById("absent").reset();
                            setTimeout(function () {
                                $('#res_message').hide();
                                $('#msg_div').hide();
                                $('#sent_form').html('ABSENT');
                            }, 1000);
                        }
                    });
                }
            })
        }

        $("#form-login").submit(function(event){
            $.ajax({
                type:"POST",
                url:"{{url('login/auth')}}",
                data:$("#form-login").serializeArray(),
                success:function(res){
                    if(res.code == 200){
                        toastr.success(res.msg, 'Sukses');
                        document.location.href='{{ url("dashboard") }}';
                    } else {
                        toastr.error(res.msg, 'Gagal');
                    }
                },
                error:function(response){
                    swal("Peringatan", response.responseJSON.message,"error");
                }
            });
            event.preventDefault();
        });
    });
</script>
</body>
</html>
