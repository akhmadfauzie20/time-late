@extends('layouts.app')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">Activities</h3>
            <div class="row">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Activity</h3>
                            <hr>
                        </div>
                         @php
                             $sesi = Auth::user();
                         @endphp
                        <div class="panel-body">
                            <ul class="list-unstyled todo-list" style="margin-top: -45px; margin-left: -30px;">
                                @if($sesi['role'] == 1)
                                <li>
                                    <p>
                                        <a href="{{ route('keterlambatan.search',['q' => 'Ayzar Fachru']) }}" style="color:grey;"><span class="title">Ayzar Fachru has been late more than 5 times</span></a>
                                        <span class="short-description">Send a message to HRD for further action.</span>
                                        <span class="date">Nov 11, 2019</span>
                                    </p>
                                    <div class="controls">
                                        <a href="#"><i class="icon-software icon-software-pencil"></i></a> <a href="#"><i class="icon-arrows icon-arrows-circle-remove"></i></a>
                                    </div>
                                </li>
                                <li>
                                    <p>
                                        <a href="{{ route('keterlambatan.search',['q' => 'Firlana Saddam Pramadan']) }}" style="color:grey;"><span class="title">Firlana Saddam has been late more than 4 times</span></a>
                                        <span class="short-description">Send messages to the employee as a warning</span>
                                        <span class="date">Sep 25, 2019</span>
                                    </p>
                                    <div class="controls">
                                        <a href="#"><i class="icon-software icon-software-pencil"></i></a> <a href="#"><i class="icon-arrows icon-arrows-circle-remove"></i></a>
                                    </div>
                                </li>
                                <li>
                                    <p>
                                        <a href="{{ route('keterlambatan.search',['q' => 'Pramesh Huda']) }}" style="color: grey;"><strong>Congrats to Pramesh Huda 🎉</strong></a>
                                        <span class="short-description">Huda has been awarded because he was never late in the past year</span>
                                        <span class="date">Dec 30, 2018</span>
                                    </p>
                                    <div class="controls">
                                        <a href="#"><i class="icon-software icon-software-pencil"></i></a> <a href="#"><i class="icon-arrows icon-arrows-circle-remove"></i></a>
                                    </div>
                                </li>

                                @elseif($sesi['id_user'] == 18)
                                <li>
                                    <p>
                                        <a href="{{ route('keterlambatan.search',['q' => 'Firlana Saddam Pramadan']) }}" style="color:grey;"><span class="title">Firlana Saddam has been late more than 4 times</span></a>
                                        <span class="short-description" style="margin-bottom: 5px;">Hi {{ $sesi['username'] }}, you get a warning from the application, because you are late more than 4 times.</span>
                                        <span class="date">Sep 25, 2019</span>
                                    </p>
                                    <div class="controls">
                                        <a href="#"><i class="icon-software icon-software-pencil"></i></a> <a href="#"><i class="icon-arrows icon-arrows-circle-remove"></i></a>
                                    </div>
                                </li>

                                @elseif($sesi['id_user'] == 7)
                                <li>
                                    <p>
                                        <a href="{{ route('keterlambatan.search',['q' => 'Ayzar Fachru']) }}" style="color:grey;"><span class="title">Ayzar Fachru has been late more than 5 times</span></a>
                                        <span class="short-description" style="margin-bottom: 5px;">Hi {{ $sesi['username'] }}, unfortunately you have received a warning letter from the company, please contact your HRD right now.</span>
                                        <span class="date">Nov 11, 2019</span>
                                    </p>
                                    <div class="controls">
                                        <a href="#"><i class="icon-software icon-software-pencil"></i></a> <a href="#"><i class="icon-arrows icon-arrows-circle-remove"></i></a>
                                    </div>
                                </li>

                                @endif
                            </ul>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
                        @endsection
