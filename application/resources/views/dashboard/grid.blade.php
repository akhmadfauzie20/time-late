@extends('layouts.app')
@section('content')

    <!-- MAIN CONTENT -->
    <?php
    //dd($bulan)
    ?>
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">Dashboard</h3>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <select id="year" name="year" class="select form-control">
                                <option value="" hidden="">Pilih Tahun</option>
                                @foreach($tahun as $datas)
                                    <option id="chooseYear" value="{{$datas->tahun}}">{{$datas->tahun}}</option>
                                @endforeach
                            </select>
                        </div>
                        @if($terlambat->count())
                            <table class="table table-shrink" style="margin-bottom:10px">
                                <canvas id="chart-1" style="width: 100%;" height="380">
                                    <p>Menit</p>
                                </canvas>
                            </table>
                    </div>
                    <hr>
                    @else
                        &nbsp&nbsp&nbsp<i class="fa fa-exclamation-triangle"></i> Data Tidak Tersedia
                    @endif
                </div>
                <!-- END MAIN CONTENT -->
                <script>
                    $(document).ready(function () {
                        var chart1 = document.getElementById('chart-1');
                        // function getRandomColor() {
                        //     var letters = '0123456789ABCDEF'.split('');
                        //     var color = '#';
                        //     for (var i = 0; i < 6; i++ ) {
                        //         color += letters[Math.floor(Math.random() * 10)];
                        //     }
                        //     return color;
                        // }

                        var chart1Obj = new Chart(chart1, {
                            type: 'bar',
                            data: {
                                labels: ['Statistik keterlambatan'],
                                datasets: [
                                        @foreach($bulan as $index => $data)
                                    {
                                        label: '{{$data}}',
                                        data: ["{{ $terlambat2->pluck('telat')[$index] }}"],
                                        backgroundColor: ['rgb(38, 116, 240)'],
                                        borderWidth: 1
                                    },
                                    @endforeach
                                ]
                            },
                            options: {
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                                },
                                onClick: function (e, a, b, c) {
                                    console.log(a, b, c);
                                }
                            }
                        });
                        $('select').find('option').click(function (e) {
                            e.preventDefault();
                            year = e.currentTarget.getAttribute('value');

                            window.location.href = "{{ route('dashboard') }}?year=" + year
                        })
                    })

                    $(".select option").val(function(idx, val) {
                        $(this).siblings('[value="' + val + '"]').remove();
                        return val;
                    });
                </script>
@endsection
