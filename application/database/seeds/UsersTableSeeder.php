<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('user')->truncate();
        DB::table('users')->insert([
            [
            'username' => 'admin',
            'email' => 'admin@timelate.com',
            'password' => bcrypt('energeek'),
            // 'password' => md5('3n3rg33k'.md5('energeek')),
            'role' => 1,
            ]
        ]);

    }
}
