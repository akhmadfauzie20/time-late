<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTerlambatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terlambats', function (Blueprint $table) {
            $table->increments('id_keterlambatan');
            $table->integer('id')->unsigned();
            $table->integer('tanggal');
            $table->integer('bulan');
            $table->integer('tahun');
            $table->integer('telat');
            $table->timestamps();

            $table->foreign('id')->references('id')->on('pegawai')->onDelete('CASCADE')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terlambats');
    }
}
