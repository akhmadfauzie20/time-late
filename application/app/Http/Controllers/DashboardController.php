<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Terlambat;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Kyslik\ColumnSortable\Sortable;
use Charts;
use DB;

class DashboardController extends Controller
{
    public function index(Request $request)
    {

        $bulan = [];
        $terlambat = Terlambat::query();
        $terlambat2 = Terlambat::select(DB::raw('bulan, sum(telat) as telat'))
            ->groupBy('bulan')->get();

        //        dd($terlambat2->pluck('bulan'));
        $huruf = [
            "","Januari","Februari","Maret","April",
            "Mei","Juni","Juli","Agustus","September",
            "Oktober","November","Desember"
        ];
        $tahun = $terlambat->get();

        if ($request->has('year')) {
            $terlambat->first('tahun', $request->get('year'))
                ->orderBy('bulan','asc');
        }

        foreach($terlambat2->pluck('bulan') as $index){
            $bulan[] = $huruf[$index];
        }

//        dd(@$bulan);

        $data = [
            'tahun' => $tahun,
            'data' => $terlambat,
            'bulan' => $bulan,
            'terlambat2' => $terlambat2
        ];

        return view('dashboard.grid', $data, compact('terlambat2','bulan','tahun','terlambat'));
    }

    public function activity()
    {
        return view('dashboard.activity');
    }
}
