<?php

namespace App\Http\Controllers;

use App\Models\Terlambat;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Pegawai;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Validator;

class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
     {
         $data = DB::table('pegawai')
              ->latest('pegawai.jabatan')
              ->paginate(10);
         return view('pegawai.grid', ['data' => $data]);
    }

    public function bulk(Request $request)
    {
        $pegawai = Pegawai::join('users','users.username','=', 'pegawai.name')
            ->select('users.*','pegawai.*');
        $q = $request->q;

        if($request->q)
        {
            $pegawai->where('pegawai.name', 'LIKE',"%$q%")
                ->orWhere('pegawai.jabatan','LIKE',"%$q%");
        }

        $results = $pegawai
            ->orderBy('pegawai.name','asc')
            ->paginate(10);

        if(count($results) > 0)
        {
            return view('pegawai.pegawai-bulk',[
                'datas' => $results
            ]);
        }
    }

    public function indexOf(Request $request)
    {
        $pegawai = Pegawai::query();
        $q = $request->q;

        if($request->q)
        {
            $pegawai->where('pegawai.name', 'LIKE',"%$q%")
                    ->orWhere('pegawai.jabatan','LIKE',"%$q%");
        }

        $results = $pegawai
                    ->orderBy('pegawai.name','asc')
                    ->paginate(10);

        if(count($results) > 0)
        {
            return view('pegawai.grid',[
           'data' => $results
            ]);
        } else {
            alert()->error('Data Tidak Ada!','Error')->persistent('Ok');
            return view('pegawai.grid',[
                'data' => $results
            ]);
        }
    }

    public function create()
    {
        return view('pegawai.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pegawai = new Pegawai();
        $pegawai->name = $request->input('name');
        $pegawai->jabatan = $request->input('jabatan');
        $pegawai->password = $request->input('password');
        $pegawai->UID = $request->input('uid');
        $file = $request->file('file');
        $ext = $file->getClientOriginalExtension();
        $newName = $pegawai->name . "-" . date('ymd') . "." . $ext;
        $file->move('uploads/file', $newName);
        $pegawai->file = $newName;
        $pegawai->save();

        $name = request('name');
        $email = request('email');
        $role = request('role');
        $password = request('password');

        $cek = User::where('username','=',$name)
                   ->orWhere('email','=',$email);

//        dd($cek->count() > 0);

        if($cek->count() > 0) {
            alert()->error('Data User Telah tersedia!', 'Gagal')->persistent('Close');
        } else {
            $user = new User;
            $user->id_user = $pegawai->id;
            $user->username = $request->input('name');
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('password'));
            $user->role = $request->input('role');
            $user->save();

            alert()->success('Data Berhasil Dibuat!', 'Sukses')->persistent('Close');
        }

        return redirect()->route('pegawai');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $data = Pegawai::where('id', $id)
             ->join('users','users.username','=', 'pegawai.name')
             ->select('users.*','pegawai.*')
             ->get();

//         dd($data);
//         $user = User::all();
//        dd($user);
         return view('pegawai.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd($request->file);
        $pegawai = Pegawai::findOrFail($id);
        $pegawai->UID = $request->input('uid');
        $pegawai->name = $request->input('name');
        $pegawai->jabatan = $request->input('jabatan');
        $pegawai->password = $request->input('password');

        $user = User::findOrFail($id);
        $user->username = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));

//        dd($user);

        if($request->hasFile('file'))
        {
            $file = $request->file('file');
            $ext = $file->getClientOriginalExtension();
            $newName = $pegawai->name . "-" . date('ymd') . "." . $ext;
            $file->move('uploads/file',$newName);

            $pegawai->file = $newName;
//            $pegawai->save();
            alert()->success('Data Berhasil Diubah!', 'Sukses')->persistent('Close');
            return redirect()->route('pegawai');
        } else {

            $pegawai->file = $pegawai->file;
        }

//        dd($pegawai);

        if ($pegawai->save()){
//            dd($pegawai->save());
            $user->save();
            alert()->success('Data Berhasil Diubah!', 'Sukses')->persistent('Close');
            return redirect()->route('pegawai');
        } else {
            alert()->error('Telah Terjadi kesalahan!', 'Gagal')->persistent('Close');
            return redirect()->route('pegawai');
        }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pegawai = Pegawai::findOrFail($id);
        if ($pegawai->delete())
        {
            $user = User::findOrFail($id);
            $user->delete();
            alert()->success('Data Berhasil Dihapus!', 'Sukses')->persistent('Close');
            return redirect()->route('pegawai');
        } else {
            alert()->error('Data Berhasil Dihapus!', 'Sukses')->persistent('Close');
            return redirect()->route('pegawai');
        }

    }
}
