<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Terlambat;
use App\Models\Pegawai;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Validator;


class KeterlambatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

         $data = Terlambat::join('pegawai','pegawai.id','=','terlambats.id')
            ->select('pegawai.*','terlambats.*')
            ->latest('terlambats.created_at')
            ->paginate(10);

         return view('keterlambatan.grid',['data' => $data]);
    }

    public function bulk(Request $request)
    {
        $terlambats = Terlambat::join('pegawai','pegawai.id','=','terlambats.id')
                              ->select('pegawai.*','terlambats.*');

        $tahun = $request->tahun;
        $q = $request->q;

        if($request->q) {
            $terlambats
                ->where('pegawai.name', 'LIKE', "%$q%")
                ->orWhere('pegawai.jabatan', 'LIKE', "%$q%")
                ->orWhere('terlambats.bulan', 'LIKE', "%$q%")
                ->orWhere('terlambats.tahun', 'LIKE', "%$q%")
                ->orWhere('terlambats.telat', 'LIKE', "%$q%");
        }


        if($request->tahun)
        {
            $terlambats->where('terlambats.tahun', $request->tahun);
        }
        $results = $terlambats
            ->latest('terlambats.created_at')
            ->paginate(10);

        if(count($results) > 0)
        {
            return view('keterlambatan.keterlambatan-bulk',[
                'datas' => $results,
                'tahun' => $tahun
            ]);
        }
    }

    public function indexOf(Request $request)
    {
        $terlambat = Terlambat::join('pegawai','pegawai.id','=','terlambats.id')
                              ->select('pegawai.*','terlambats.*');

        $tahun = $request->tahun;
        $q = $request->q;

        if($request->q) {
            $terlambat
                ->where('pegawai.name', 'LIKE', "%$q%")
                ->orWhere('pegawai.jabatan', 'LIKE', "%$q%")
                ->orWhere('pegawai.UID', 'LIKE', "%$q%")
                ->orWhere('terlambats.bulan', 'LIKE', "%$q%")
                ->orWhere('terlambats.tahun', 'LIKE', "%$q%")
                ->orWhere('terlambats.telat', 'LIKE', "%$q%");
//                ->orderBy('terlambats.created_at');
        }


        if($request->tahun)
        {
            $terlambat->where('terlambats.tahun', $request->tahun);
        }
        $results = $terlambat
                        ->orderBy('terlambats.created_at')
                        ->paginate(10);

        if(count($results) > 0)
        {
            return view('keterlambatan.grid',[
               'data' => $results,
               'tahun' => $tahun
            ]);
        } else {
            alert()->error('Data Tidak Ada!','Error')->persistent('Ok');
            return view('keterlambatan.grid',[
                'data' => $results
            ]);
        }
    }

    public function edit($id){
        $data = Terlambat::where('id_keterlambatan',$id)
        ->join('pegawai','pegawai.id','=','terlambats.id')
        ->select('pegawai.*','terlambats.*')->get();
        $pegawai = Pegawai::all();
        return view('keterlambatan.edit',compact('data','pegawai'));
    }

    public function create()
    {
         $data = Pegawai::get();
        return view('keterlambatan.create',compact('data'));
    }


    public function absent(Request $request)
    {
        //      dd($request);

        $data = new Terlambat();
        $pegawai = Pegawai::where('UID', $request->uid)
                        ->where('password', $request->password)
                        ->first();
        $cek = Pegawai::where('UID','=',$request->uid)
                      ->orWhere('password','=',$request->password);

//        if ($cek->count() < 0) {
//            $arr = array('msg' => 'User Belum Tersedia!');
//        } elseif ($pegawai->count() > 0) {
//            $arr = array('msg' => 'Anda Telah Absen!');
//        } else {

        date_default_timezone_set('Asia/Jakarta');
        if(strtotime(date('H:i:s')) > strtotime('07:00:00'))
        {
            $date = abs(strtotime(date('h:i:s')) - strtotime('07:00:00'));
            $date /= 60;

            $data->id = $pegawai->id;
            $data->tanggal = date('d');
            $data->bulan = date('m');
            $data->tahun = date('Y');
            $data->telat = strval(intval($date));
            $data->save();

            $arr = array('msg' => 'Anda Berhasil Absen, Anda terlambat!');

        } else {
            $data->id = $pegawai->id;
            $data->tanggal = date('d');
            $data->bulan = date('m');
            $data->tahun = date('Y');
            $data->telat = 0;

            $arr = array('msg' => 'Anda Berhasil Absen, Anda tidak terlambat!');
        }

            return Response()->json($arr);
//    }
}
    public function store(Request $request)
    {
        $name = request('name');
        $tanggal = request('tanggal');
        $bulan = request('bulan');
        $tahun = request('tahun');

        $cek = Terlambat::where('id',$name)
                        ->orWhere('tanggal',$tanggal)
                        ->orWhere('bulan',$bulan)
                        ->orWhere('tahun',$tahun);

        if($cek->count()){
             alert()->error('Anda Telah Absen!', 'Gagal')->persistent('Close');
        } else {
            Terlambat::create([
        'id'=>request('name'),
        'tanggal'=>request('tanggal'),
        'bulan'=>request('bulan'),
        'tahun'=>request('tahun'),
        'telat'=>request('telat')
        ]);
        alert()->success('Data Berhasil Ditambahkan!', 'Sukses')->persistent('Close');
        }
        return redirect()->route('keterlambatan');
    }

    public function update(Request $request, $id_keterlambatan)
    {
        $name = request('name');
        $tanggal = request('tanggal');
        $bulan = request('bulan');
        $tahun = request('tahun');

        $cek = Terlambat::where([
            ['id',$name],
            ['tanggal',$tanggal],
            ['bulan',$bulan],
            ['tahun',$tahun]
        ]);

        if($cek->count()){
             alert()->error('Data Sudah Ada!', 'Gagal')->persistent('Close');
        }else{
        $data = Terlambat::where('id_keterlambatan',$id_keterlambatan)->first();
        $data->id = $request->name;
        $data->tanggal = $request->tanggal;
        $data->bulan = $request->bulan;
        $data->tahun = $request->tahun;
        $data->telat = $request->telat;
        $data->save();
         alert()->success('Data Berhasil Diubah!', 'Sukses')->persistent('Close');
        }
        return redirect()->route('keterlambatan');
    }

    public function destroy($id_keterlambatan)
    {
        $data = Terlambat::where('id_keterlambatan',$id_keterlambatan)->first();
        $data->delete();
        alert()->success('Data Berhasil Dihapus!', 'Sukses')->persistent('Close');
        return redirect('keterlambatan');
    }
}
