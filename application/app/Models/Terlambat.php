<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Sortable;
use Eloquent;

class Terlambat extends Model
{
    protected $table = 'terlambats';
	protected $primaryKey = 'id_keterlambatan';
	protected $fillable = [
					'id_keterlambatan',
					'id',
                    'tanggal',
                    'bulan',
                    'tahun',
                    'telat',
					'created_at',
					'created_by',
					'updated_at',
					'updated_by',
					'deleted_at',
					'deleted_by'];

	protected $hidden = [
					'created_at',
					'created_by',
					'updated_at',
					'updated_by',
					'deleted_at',
					'deleted_by'];
}
