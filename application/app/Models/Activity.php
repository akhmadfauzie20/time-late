<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = 'activity';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'title',
        'content',
        'created_at'
    ];

    protected $hidden = [
        'created_at'
    ];
}
