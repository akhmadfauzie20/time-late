<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Eloquent;

class Pegawai extends Model
{
    protected $table = 'pegawai';
    protected $primaryKey = 'id';
    protected $fillable = [
					'id',
					'name',
					'jabatan',
					'file',
					'created_at',
					'created_by',
					'updated_at',
					'updated_by',
					'deleted_at',
					'deleted_by'];

	protected $hidden = [
					'created_at',
					'created_by',
					'updated_at',
					'updated_by',
					'deleted_at',
					'deleted_by'];
}
